<?php
/**
 * User: shutkos
 * Date: 22.06.2015
 * Time: 16:53
 * Example:
 * $db = DataBasePDO::getInstance();
 * $res = $db->select("user_login", ["*"], ["login" => "Vasya", "AND", "pass" => "123", "AND", "latest_date" => "0000-00-00"]);
 * $db->update("user_login", ["pass" => "111"], ["login" => "dasha", "AND", "latest_ip"=>"0.0.0.0"]);
 *
 */
class DataBasePDO
{
    private $_pdo;
    static $_instance = null;

    private function __construct($file = 'config.ini')
    {
        if (!$settings = parse_ini_file($file, true)) die('Не могу открыть файл.');
        $dns = $settings['dns']['driver'] . ':host=' . $settings['dns']['host'] . ';';
        $dns .= "dbname=" . $settings['db']['db_name'] . ';';
        $dns .= "charset=" . $settings['dns']['charset'];
        $user = $settings['db']['db_user'];
        $pass = $settings['db']['db_pass'];

        $options = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );

        $this->_pdo = new PDO($dns, $user, $pass, $options);
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        try {
            if (!self::$_instance instanceof self) {
                self::$_instance = new self();
            }
            return self::$_instance;
        } catch (PDOException $e) {
            die('Подключение не удалось: ' . $e->getMessage() . $e->errorInfo);
        }
    }

    /**
     * Метод проверяет тип данных
     * @param $test - проверяемый параметр
     * @param $type - номер типа :
     * -1 array
     * -2 string
     * -3 integer
     * -4 booleam
     */
    private function checkType($test, $type)
    {
        switch ($type) {
            case 1:
                if (is_array($test)) {
                    continue;
                } else {
                    die('Not array!');
                }
            case 2:
                if (is_string($test)) {
                    continue;
                } else {
                    die('Not a string!');
                }
            case 3:
                if (is_int($test)) {
                    continue;
                } else {
                    die('Not a integer!');
                }
            case 4:
                if (is_bool($test)) {
                    continue;
                } else {
                    die('Not a boolean!');
                }
            default:
                die();
        }
    }

    /**
     * Метод проверяет есть ли в массиве элемент со значением OR / AND
     * если есть удаляет его из execute
     * @param array $array
     * @return array
     */
    private function executeAndOr($array = [])
    {
        $arr = array_values($array);
        for ($i = 0; $i <= count($arr); $i++) {
            if ($arr[$i] == "OR" || $arr[$i] == "AND") {
                unset($arr[$i]);
            }
        }
        return $arr;
    }

    /**
     * Метод строит строку WHERE для запроса к БД
     * @param array $where
     * @return string
     */
    private function where($where = array())
    {
        $this->checkType($where, 1);
        reset($where);
        $first = key($where);
        foreach ($where AS $key => $value) {
            static $i = 0;
            if ($key === $first) {
                $sql .= " WHERE `$key` = ?";
            } else {
                switch ($key) {
                    case ($key == $i && $value == "AND"):
                        $sql .= " AND ";
                        $i++;
                        break;
                    case ($key == $i && $value == "OR"):
                        $sql .= " OR ";
                        $i++;
                        break;
                    default:
                        $sql .= " `$key` = ?";
                }
            }
        }
        return $sql;
    }

    public function select($table, $what = array(), $where = array(), $order = array(), $limit = array())
    {
        $this->checkType($what, 1);
        if (count($what) == 1 && $what[0] === "*") {
            $sql = "SELECT " . implode("", $what) . " FROM `" . $table . "`";
        } else {
            $sql = "SELECT `" . implode("`, `", $what) . "` FROM `" . $table . "`";
        }
        if (!empty($where)) {
            $sql .= $this->where($where);
        }
        if (!empty($order)) {
            $this->checkType($order, 1);
            $sql .= " ORDER BY `" . $order[0] . "`";
            $sql .= ($order[1] == "true") ? " ASC " : " DESC ";
        }
        if (!empty($limit)) {
            $this->checkType($limit, 1);
            $sql .= " LIMIT " . implode(", ", $limit);
        }
        $query = $this->_pdo->prepare($sql);
        $res = $this->executeAndOr($where);
        $query->execute(array_values($res));
        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    public function insert($table, $data = array())
    {
        $this->checkType($data, 1);
        $str = "";
        $counter = count($data);
        while ($counter > 0) {
            if ($counter == 1) {
                $str .= "?";
            } else {
                $str .= "?, ";
            }
            $counter--;
        }
        $sql = "INSERT INTO `" . $table . "` (`" . implode("`, `", array_keys($data)) . "`) VALUES ($str)";
        $query = $this->_pdo->prepare($sql);
        $query->execute(array_values($data));
    }

    public function update($table, $data = array(), $where = array())
    {
        $this->checkType($data, 1);
        $sql = "UPDATE `" . $table . "` SET ";
        foreach ($data AS $key => $value) {
            reset($data);
            $first = key($data);
            if ($key == $first) {
                $sql .= "`$key` = ?";
            } else {
                $sql .= ", `$key` = ?";
            }
        }
        $sql .= $this->where($where);
        $query = $this->_pdo->prepare($sql);
        $res = $this->executeAndOr($where);
        $arr = array_merge(array_values($data), array_values($res));
        $query->execute($arr);
    }

    public function delete($table, $where = array())
    {
        $sql = "DELETE FROM `" . $table . "` ";
        $sql .= $this->where($where);
        $query = $this->_pdo->prepare($sql);
        $query->execute(array_values($where));
    }
}
